import { PrismaClient } from "@prisma/client";
import * as bcrypt from "bcrypt";

const prisma = new PrismaClient();

async function main(): Promise<void> {
    const admin = await prisma.user.upsert({
        where: { email: "admin@example.com" },
        update: {},
        create: {
            email: "admin@example.com",
            name: "Admin",
            password: await bcrypt.hash("admin", 10),
            roles: ["Admin"],
            accessToken: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFkbWluQGV4YW1wbGUuY29tIiwiaWF0IjoxNjY4NzgwODIwLCJleHAiOjE2Njg3ODA4ODB9.C5HlLp4T85R1igaFntmher1Fgci_71I9gPhS76zqrCU",
            banned: false,
            banReason: ""
        }
    });

    const user = await prisma.user.upsert({
        where: { email: "user@example.com" },
        update: {},
        create: {
            email: "user@example.com",
            name: "User",
            password: await bcrypt.hash("user", 10),
            accessToken: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InVzZXJAZXhhbXBsZS5jb20iLCJpYXQiOjE2Njg3ODA4MjAsImV4cCI6MTY2ODc4MDg4MH0.ihJDx8upGYBnR7b8RUb7A-Ivd786PpUPQAdXhroM460",
            banned: false,
            banReason: ""
        }
    });

    const post1 = await prisma.post.upsert({
        where: { id: 1 },
        update: {},
        create: {
            title: "Post 1",
            published: true,
            authorId: 2
        }
    });

    const post2 = await prisma.post.upsert({
        where: { id: 2 },
        update: {},
        create: {
            title: "Post 2",
            published: false,
            authorId: 2
        }
    });

    console.log({ admin, user, post1, post2 });
}

main()
    .catch((e) => {
        console.log(e);
        process.exit(1);
    })
    .finally(async () => {
        await prisma.$disconnect();
    });
