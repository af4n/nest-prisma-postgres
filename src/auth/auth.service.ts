import { Injectable, UnauthorizedException } from "@nestjs/common";
import { PrismaService } from "../prisma/prisma.service";
import { AuthDto } from "./response/auth.dto";
import * as bcrypt from "bcrypt";

@Injectable()
export class AuthService {
    constructor(private prisma: PrismaService) {
    }

    async login(email: string, password: string): Promise<AuthDto> {
        const user = await this.prisma.user.findUnique({
            where: { email: email }
        });

        const isMatch = await bcrypt.compare(password, user.password);

        if (!user || !isMatch) {
            throw new UnauthorizedException("Invalid email or password");
        }

        return {
            accessToken: user.accessToken
        };
    }
}
