import { ApiOkResponse, getSchemaPath } from '@nestjs/swagger';
import { Page } from './page.dto';
import { applyDecorators, Type } from '@nestjs/common';

export const ApiPageResponse = <TModel extends Type<any>>(model: TModel) => {
    return applyDecorators(
        ApiOkResponse({
            schema: {
                title: `PageResponseOf${model.name}`,
                allOf: [
                    { $ref: getSchemaPath(Page) },
                    {
                        properties: {
                            edges: {
                                type: 'array',
                                title: `EdgeOf${model.name}`,
                                items: {
                                    type: 'object',
                                    required: ['cursor', 'node'],
                                    properties: {
                                        cursor: { type: 'string' },
                                        node: {
                                            type: 'object',
                                            $ref: getSchemaPath(model),
                                        },
                                    },
                                },
                            },
                        },
                    },
                ],
            },
        }),
    );
};
