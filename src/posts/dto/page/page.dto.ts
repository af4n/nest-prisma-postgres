import { Edge } from './edge.dto';
import { PageInfo } from './page-info.dto';
import { ApiProperty } from '@nestjs/swagger';

export class Page<Record> {
    edges: Edge<Record>[];

    @ApiProperty()
    pageInfoInfo: PageInfo;

    @ApiProperty()
    totalCount: number;
}
