import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Patch,
    Post,
    UseGuards
} from "@nestjs/common";
import { ApiBearerAuth, ApiOkResponse, ApiTags } from "@nestjs/swagger";
import { UsersService } from "./users.service";
import { CreateUserDto } from "./dto/request/create-user.dto";
import { UpdateUserDto } from "./dto/request/update-user.dto";
import { UserDto } from "./dto/response/user.dto";
import { UserDetailDto } from "./dto/response/user-detail.dto";
import { Roles } from "src/auth/roles.decorator";
import { Role } from "@prisma/client";
import { RolesGuard } from "src/auth/roles.guard";
import { JwtAuthGuards } from "../auth/jwt-auth.guards";
import { AddRoleDto } from "./dto/request/add-role.dto";
import { BanUserDto } from "./dto/request/ban-user.dto";

@Controller("users")
@ApiTags("users")
export class UsersController {
    constructor(private readonly usersService: UsersService) {
    }

    @Post()
    @ApiOkResponse({ type: UserDto })
    create(@Body() createUserDto: CreateUserDto) {
        return this.usersService.create(createUserDto);
    }

    @Get()
    @Roles(Role.User)
    @UseGuards(RolesGuard)
    @ApiOkResponse({ type: UserDto, isArray: true })
    findAll() {
        return this.usersService.findAll();
    }

    @Get(":id")
    @UseGuards(JwtAuthGuards)
    @ApiBearerAuth()
    @ApiOkResponse({ type: UserDetailDto })
    findOne(@Param("id") id: string) {
        return this.usersService.findOne(+id);
    }

    @Patch(":id")
    @ApiOkResponse({ type: UserDto })
    update(@Param("id") id: string, @Body() updateUserDto: UpdateUserDto) {
        return this.usersService.update(+id, updateUserDto);
    }

    @Delete(":id")
    @ApiOkResponse({ type: UserDto })
    remove(@Param("id") id: string) {
        return this.usersService.remove(+id);
    }

    @Patch(":id/role")
    @Roles(Role.Admin)
    @UseGuards(RolesGuard)
    @ApiOkResponse({ type: UserDto })
    addRole(@Param("id") id: string, @Body() addRoleDto: AddRoleDto) {
        return this.usersService.addRole(+id, addRoleDto);
    }

    @Patch(":id/ban")
    @Roles(Role.Admin)
    @UseGuards(RolesGuard)
    @ApiOkResponse({ type: UserDto })
    ban(@Param("id") id: string, @Body() banUserDto: BanUserDto) {
        return this.usersService.ban(+id, banUserDto);
    }
}
