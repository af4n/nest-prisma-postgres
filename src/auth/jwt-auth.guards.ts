import {
    CanActivate,
    ExecutionContext,
    Injectable,
    UnauthorizedException
} from "@nestjs/common";
import { Observable } from "rxjs";
import { PrismaService } from "../prisma/prisma.service";

@Injectable()
export class JwtAuthGuards implements CanActivate {
    constructor(private prisma: PrismaService) {
    }

    // @ts-ignore
    async canActivate(context: ExecutionContext): Promise<boolean | Promise<boolean> | Observable<boolean>> {
        const request = context.switchToHttp().getRequest();
        try {
            const authHeader = request.headers.authorization;
            const token = authHeader.split(" ")[1];
            const user = await this.prisma.user.findUnique({
                where: { accessToken: token }
            });
            if (user) return true;
        } catch (e) {
            throw new UnauthorizedException("User is not authorized");
        }
    }
}
