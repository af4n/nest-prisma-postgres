import { Injectable, NotFoundException } from "@nestjs/common";
import { CreateUserDto } from "./dto/request/create-user.dto";
import { UpdateUserDto } from "./dto/request/update-user.dto";
import { PrismaService } from "../prisma/prisma.service";
import * as bcrypt from "bcrypt";
import { JwtService } from "@nestjs/jwt";
import { AddRoleDto } from "./dto/request/add-role.dto";
import { BanUserDto } from "./dto/request/ban-user.dto";


@Injectable()
export class UsersService {
    constructor(private prisma: PrismaService, private jwtServise: JwtService) {
    }

    async create(createUserDto: CreateUserDto) {
        const { email, name, password } = createUserDto;
        const hash = await bcrypt.hash(password, 10);
        const token = this.jwtServise.sign({ email });
        return await this.prisma.user.create({
            data: {
                email,
                name,
                password: hash,
                accessToken: token
            },
            select: {
                id: true,
                email: true,
                name: true,
                roles: true
            }
        });
    }

    async findAll() {
        return await this.prisma.user.findMany({
            select: {
                id: true,
                email: true,
                name: true,
                roles: true
            }
        });
    }

    async findOne(id: number) {
        const user = await this.prisma.user.findUnique({
            where: { id },
            select: {
                id: true,
                email: true,
                name: true,
                roles: true,
                posts: true
            }
        });

        if (!user) {
            throw new NotFoundException("User could not be found");
        }

        return user;
    }

    async update(id: number, updateUserDto: UpdateUserDto) {
        return await this.prisma.user.update({
            where: { id },
            data: updateUserDto,
            select: {
                id: true,
                email: true,
                name: true,
                roles: true
            }
        });
    }

    async remove(id: number) {
        return await this.prisma.user.delete({
            where: { id },
            select: {
                id: true,
                email: true,
                name: true,
                roles: true
            }
        });
    }

    async addRole(id: number, addRoleDto: AddRoleDto) {
        const { value } = addRoleDto;
        const user = await this.prisma.user.findUnique({
            where: { id }
        });
        const updateUser = await this.prisma.user.update({
            where: { id },
            data: { roles: [...user.roles, value] },
            select: {
                id: true,
                email: true,
                name: true,
                roles: true
            }
        });
        if (!updateUser) {
            throw new NotFoundException("User could not be found");
        }
        return updateUser;
    }

    async ban(id: number, banUserDto: BanUserDto) {
        const { banReason } = banUserDto;
        const user = await this.prisma.user.update({
            where: { id },
            data: {
                banned: true,
                banReason
            },
            select: {
                id: true,
                email: true,
                name: true,
                roles: true
            }
        });
        if (!user) {
            throw new NotFoundException("User could not be found");
        }
        return user;
    }
}
