import { ApiProperty, PartialType } from '@nestjs/swagger';
import { PostDto } from 'src/posts/dto/response/post.dto';
import { UserDto } from './user.dto';

export class UserDetailDto extends PartialType(UserDto) {
    @ApiProperty({ type: [PostDto] })
    posts: PostDto[];
}
