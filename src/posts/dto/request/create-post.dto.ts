import { ApiProperty } from '@nestjs/swagger';
import {
    IsBoolean,
    IsInt,
    IsNotEmpty,
    IsOptional,
    MaxLength,
    Min,
    MinLength,
} from 'class-validator';

export class CreatePostDto {
    @IsNotEmpty()
    @MinLength(5)
    @MaxLength(255)
    @ApiProperty()
    title: string;

    @IsOptional()
    @IsBoolean()
    @ApiProperty({ required: false, default: false })
    published?: boolean;

    @IsNotEmpty()
    @IsInt()
    @Min(1)
    @ApiProperty()
    authorId: number;
}
