import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, MaxLength, MinLength } from 'class-validator';

export class CreateUserDto {
    @IsNotEmpty()
    @IsEmail()
    @ApiProperty()
    email: string;

    @IsNotEmpty()
    @MinLength(5)
    @MaxLength(255)
    @ApiProperty()
    name: string;

    @IsNotEmpty()
    @MinLength(5)
    @MaxLength(255)
    @ApiProperty()
    password: string;
}
