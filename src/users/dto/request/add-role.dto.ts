import { ApiProperty } from "@nestjs/swagger";
import { Role } from "@prisma/client";
import { IsNotEmpty, IsString } from "class-validator";

export class AddRoleDto {
    @IsNotEmpty()
    @IsString()
    @ApiProperty()
    value: Role;
}
