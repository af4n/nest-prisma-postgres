import {
    CanActivate,
    ExecutionContext,
    HttpException,
    HttpStatus,
    Injectable
} from "@nestjs/common";
import { Observable } from "rxjs";
import { PrismaService } from "../prisma/prisma.service";
import { Reflector } from "@nestjs/core";
import { ROLES_KEY } from "./roles.decorator";

@Injectable()
export class RolesGuard implements CanActivate {
    constructor(private prisma: PrismaService, private reflector: Reflector) {
    }

    // @ts-ignore
    async canActivate(context: ExecutionContext): Promise<boolean | Promise<boolean> | Observable<boolean>> {
        try {
            const requiredRoles = this.reflector.getAllAndOverride<string[]>(ROLES_KEY, [
                context.getHandler(),
                context.getClass()
            ]);
            console.log("requiredRoles", requiredRoles);
            if (!requiredRoles) {
                return true;
            }
            const request = context.switchToHttp().getRequest();
            const authHeader = request.headers.authorization;
            const token = authHeader.split(" ")[1];
            const user = await this.prisma.user.findUnique({
                where: { accessToken: token }
            });
            return user.roles.some(role => requiredRoles.includes(role));
        } catch (e) {
            throw new HttpException("Forbidden resource", HttpStatus.FORBIDDEN);
        }
    }
}
