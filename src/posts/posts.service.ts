import { Injectable, NotFoundException } from '@nestjs/common';
import { CreatePostDto } from './dto/request/create-post.dto';
import { UpdatePostDto } from './dto/request/update-post.dto';
import { PrismaService } from '../prisma/prisma.service';
import { findManyCursorConnection } from '@devoxa/prisma-relay-cursor-connection';
import { Prisma } from '@prisma/client';
import { ConnectionArgsDto } from './dto/page/connection-args.dto';

@Injectable()
export class PostsService {
    constructor(private prisma: PrismaService) {}

    create(createPostDto: CreatePostDto) {
        const { title, published, authorId } = createPostDto;
        return this.prisma.post.create({
            data: {
                title,
                published,
                author: {
                    connect: { id: authorId },
                },
            },
        });
    }

    findAll() {
        return this.prisma.post.findMany({
            where: { published: true },
            include: {
                author: {
                    select: {
                        id: true,
                        email: true,
                        name: true,
                        roles: true,
                    },
                },
            },
        });
    }

    async findPage(connectionArgsDto: ConnectionArgsDto) {
        const where: Prisma.PostWhereInput = { published: true };

        return await findManyCursorConnection(
            // @ts-ignore
            (args) => this.prisma.post.findMany({ ...args, where }),
            () => this.prisma.post.count({ where }),
            connectionArgsDto,
        );
    }

    findDrafts() {
        return this.prisma.post.findMany({
            where: { published: false },
            include: {
                author: {
                    select: {
                        id: true,
                        email: true,
                        name: true,
                        roles: true,
                    },
                },
            },
        });
    }

    async findOne(id: number) {
        const post = await this.prisma.post.findUnique({
            where: { id },
            include: {
                author: {
                    select: {
                        id: true,
                        name: true,
                        email: true,
                        roles: true,
                    },
                },
            },
        });

        if (!post) {
            throw new NotFoundException('Post could not be found');
        }

        return post;
    }

    update(id: number, updatePostDto: UpdatePostDto) {
        const { title, published, authorId } = updatePostDto;
        return this.prisma.post.update({
            where: { id },
            data: {
                title,
                published,
                author: {
                    connect: { id: authorId },
                },
            },
        });
    }

    remove(id: number) {
        return this.prisma.post.delete({
            where: { id },
        });
    }
}
