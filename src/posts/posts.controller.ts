import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Patch,
    Post,
    Query,
} from '@nestjs/common';
import { ApiExtraModels, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { PostsService } from './posts.service';
import { CreatePostDto } from './dto/request/create-post.dto';
import { UpdatePostDto } from './dto/request/update-post.dto';
import { PostDto } from './dto/response/post.dto';
import { ConnectionArgsDto } from './dto/page/connection-args.dto';
import { Page } from './dto/page/page.dto';
import { ApiPageResponse } from './dto/page/api-page-response';
import { PostDetailDto } from './dto/response/post-detail.dto';

@Controller('posts')
@ApiTags('posts')
@ApiExtraModels(Page)
export class PostsController {
    constructor(private readonly postsService: PostsService) {}

    @Post()
    @ApiOkResponse({ type: PostDto })
    create(@Body() createPostDto: CreatePostDto) {
        return this.postsService.create(createPostDto);
    }

    @Get()
    @ApiOkResponse({ type: PostDetailDto, isArray: true })
    findAll() {
        return this.postsService.findAll();
    }

    @Get('page')
    @ApiPageResponse(PostDto)
    async findPage(@Query() connectionArgsDto: ConnectionArgsDto) {
        return this.postsService.findPage(connectionArgsDto);
    }

    @Get('drafts')
    @ApiOkResponse({ type: PostDetailDto, isArray: true })
    findDrafts() {
        return this.postsService.findDrafts();
    }

    @Get(':id')
    @ApiOkResponse({ type: PostDetailDto })
    findOne(@Param('id') id: string) {
        return this.postsService.findOne(+id); // як правильно обробити не існуючий пост ? бо приходить 200

        // const post = this.postsService.findOne(+id);
        // if (post) return post;
        // post.status(HttpStatus.CONFLICT).json('Post could not be found');
    }

    @Patch(':id')
    @ApiOkResponse({ type: PostDto })
    update(@Param('id') id: string, @Body() updatePostDto: UpdatePostDto) {
        return this.postsService.update(+id, updatePostDto);
    }

    @Delete(':id')
    @ApiOkResponse({ type: PostDto })
    remove(@Param('id') id: string) {
        return this.postsService.remove(+id); // якщо такого поста не має приходить не зрозуміла 500 посилка
    }
}
