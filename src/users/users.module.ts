import { Module } from "@nestjs/common";
import { UsersService } from "./users.service";
import { UsersController } from "./users.controller";
import { PrismaModule } from "../prisma/prisma.module";
import { JwtModule } from "@nestjs/jwt";

export const jwtSecret = "jwtSecret2jwtSecret4jwtSecret6";

@Module({
    controllers: [UsersController],
    providers: [UsersService],
    imports: [
        PrismaModule,
        JwtModule.register({
            secret: jwtSecret,
            signOptions: { expiresIn: "60s" }
        })
    ],
    exports: [UsersService]
})
export class UsersModule {
}
