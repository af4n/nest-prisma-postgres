import { ApiProperty } from '@nestjs/swagger';

export class Edge<Record> {
    node: Record;

    @ApiProperty()
    cursor: string;
}
