import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsString } from "class-validator";

export class BanUserDto {
    @IsNotEmpty()
    @IsString()
    @ApiProperty()
    banReason: string;
}
