import { Post } from '@prisma/client';
import { ApiProperty } from '@nestjs/swagger';

export class PostDto implements Post {
    @ApiProperty()
    id: number;

    @ApiProperty()
    title: string;

    @ApiProperty()
    published: boolean;

    @ApiProperty()
    createdAt: Date;

    @ApiProperty()
    updatedAt: Date;

    @ApiProperty()
    authorId: number;
}
