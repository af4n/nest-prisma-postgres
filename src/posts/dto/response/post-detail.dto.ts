import { ApiProperty, PartialType } from '@nestjs/swagger';
import { UserDto } from 'src/users/dto/response/user.dto';
import { PostDto } from './post.dto';

export class PostDetailDto extends PartialType(PostDto) {
    @ApiProperty()
    author: UserDto;
}
